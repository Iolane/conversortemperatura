
package conversorTemperatura.controle;

public class ControleConversor {
    
      
    public double celsiusParaFahrenheit (double temperatura){
        return (9 * (temperatura / 5)) + 32 ; 
     }
     
     public double celsiusParaKelvin (double temperatura){
         return temperatura + 273 ;
     }
     
     public double fahrenheitParaCelsius (double temperatura){
         return ((temperatura - 32) / 9) * 5 ; 
     }
     
     public double kelvinParaCelsius (double temperatura){
         return temperatura - 273 ;
     }
     
     public double kelvinParaFahrenheit (double temperatura){
         return ( ( (temperatura - 273)/5) *9) + 32 ;
     }
     
     public double fahrenheitParaKelvin (double temperatura){
         
         return ( ( (temperatura - 32) / 9) * 5) + 273 ;
         
     }
}
