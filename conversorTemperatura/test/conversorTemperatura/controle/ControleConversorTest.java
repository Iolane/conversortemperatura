/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package conversorTemperatura.controle;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Iolane Caroline
 */
public class ControleConversorTest {
    
    private ControleConversor umDado;
    
    @Before
    public void setUp() throws Exception {
         umDado = new ControleConversor();
    }

  
    @Test
    public void testCelsiusParaFahrenheit() {
      assertEquals(32.0, umDado.celsiusParaFahrenheit(0.0), 0.01);
    }

    @Test
    public void testCelsiusParaKelvin() {
      assertEquals(273.0, umDado.celsiusParaKelvin(0.0), 0.01);
    }
    
    @Test
    public void testFahrenheitParaKelvin() {
      assertEquals(0.0, umDado.fahrenheitParaKelvin(-459.67), 0.01);
    }
    
    @Test
    public void testKelvinParaFahrenheit() {
      assertEquals(-459.67, umDado.kelvinParaFahrenheit(0.0), 0.01);
    }
    
    @Test
    public void testFahrenheitParaCelsius() {
      assertEquals(0.0, umDado.fahrenheitParaCelsius(32.0), 0.01);
    }
    
    @Test
    public void testKelvinParaCelsius() {
      assertEquals(0.0, umDado.kelvinParaCelsius(273.0), 0.01);
    }
    
}